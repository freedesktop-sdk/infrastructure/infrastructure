variable "auth_token" {}

# Configure the Packet Provider
provider "packet" {
  auth_token = "${var.auth_token}"
}

# Create a device and add it to the project
resource "packet_device" "gitlab_runner" {
  hostname         = "runner-arm64-freedesktop"
  plan             = "baremetal_2a"
  facility         = "ams1"
  operating_system = "debian_9"
  billing_cycle    = "hourly"
  project_id       = "41ffb2a3-9381-4013-b3b4-3330f77488a1"
}

output "runner_ip" {
  value = "${packet_device.gitlab_runner.access_public_ipv4}"
}
