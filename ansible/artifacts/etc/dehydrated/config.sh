##########################
# Dehydrated config file #
##########################

# Path to certificate authority (staging: https://acme-staging.api.letsencrypt.org/directory)
CA="https://acme-v01.api.letsencrypt.org/directory"
#CA="https://acme-staging.api.letsencrypt.org/directory"
# Which challenge should be used? Currently http-01 and dns-01 are supported
CHALLENGETYPE="http-01"

# Automatic cleanup (default: no)
AUTO_CLEANUP="yes"

# Email for registration
CONTACT_EMAIL="adam.jones@codethink.co.uk"
