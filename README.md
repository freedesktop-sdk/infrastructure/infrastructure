# Infrastructure

This repository is now archived.

For the released server please use:
 - https://gitlab.com/freedesktop-sdk/infrastructure/releases


For the cache server please use:
 - https://gitlab.com/freedesktop-sdk/infrastructure/artifacts

